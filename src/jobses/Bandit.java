package jobses;

import bang.Player;

public class Bandit extends Job {

    public Bandit(Player player) {
        super(player);
    }

    @Override
    public String getJobMission() {
		return "kill Sheriff";
    }

	@Override
	public String getJobName() {
        return "Bandit";
	}

}
