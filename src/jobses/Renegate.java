package jobses;

import bang.Player;

public class Renegate extends Job {

    public Renegate(Player player) {
        super(player);
    }

    @Override
    public String getJobName() {
        return "Renegade";
    }
    
    @Override
    public String getJobMission() {
        return "kill them all";
    }
    
}
