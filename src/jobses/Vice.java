package jobses;

import bang.Player;

public class Vice extends Job {

    public Vice(Player player) {
        super(player);
    }

    @Override
    public String getJobName() {
        return "Vice";
    }

    @Override
    public String getJobMission() {
        return "kill Bandits and Renegate";
    }


}
