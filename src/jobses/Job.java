package jobses;

import bang.Player;
import java.util.ArrayList;
import java.util.List;

abstract public class Job {

    private List<Job> targets;
    protected int maxLives = 8;
    private Player player;

    public Job(Player player) {
        targets = new ArrayList<>();
        this.player = player;
    }

    public List<Job> getTargets() {
        return targets;
    }

    public void addTarget(Job job) {
        targets.add(job);
    }

    public int getMaxLives() {
        return maxLives;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

	
    public String getJobNameAndMission() {
		return getJobName() + ": " + getJobMission();
	}
	
    public abstract String getJobName();

    public abstract String getJobMission();

}
