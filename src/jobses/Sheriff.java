package jobses;

import bang.Player;

public class Sheriff extends Job {

    public Sheriff(Player player) {
        super(player);
        maxLives++;
    }

    @Override
    public String getJobName() {
        return "Sheriff";
    }
	
    @Override
    public String getJobMission() {
        return "kill all Bandits, Renegates";
    }
}
