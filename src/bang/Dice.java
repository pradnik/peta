package bang;

import java.util.Random;

public class Dice {

    public enum Value {
        NONE, ARROW, GATLING, SHORT_SHOT, LONG_SHOT, BEER, DYNAMITE;
    }

    public static Value randomValue() {
        Random rand = new Random();
        Value[] values = Value.values();
        return values[1 + rand.nextInt(values.length - 1)];
    }
	

    private Value value;
    private boolean readyToThrow = true;

	
    public Dice() {
        reset();
    }

    public Value getValue() {
        return value;
    }

    public boolean isReadyToThrow() {
        return readyToThrow;
    }
	
    public void setReadyToThrow(boolean readyToThrow) {
        this.readyToThrow = readyToThrow;
    }

	public boolean canBeRethrown() {
		return value != Value.DYNAMITE;
	}	
	
    public void throwDice() {
        value = randomValue();
    }
    
    public boolean isNone() {
        return value == Value.NONE;
    }
	
	public void setNone() {
        value = Value.NONE;
    }

    public final void reset() {
        value = Value.NONE;
        readyToThrow = true;
    }
}
