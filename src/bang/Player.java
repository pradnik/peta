package bang;

import javax.swing.ImageIcon;
import jobses.Bandit;
import jobses.Job;
import jobses.Sheriff;
import jobses.Vice;

public class Player {

	private int livesLeft = 0;
	private int arrowCount = 0;

	private int throwsLeft = 3;
	private String name;
	private Job job;
	private boolean diceAccepted = false;
	private Dice[] dice;
	
	ImageIcon image;

	public void setImage(ImageIcon image) {
		this.image = image;
	}

	public ImageIcon getIcon() {
		return image;
	}

	public Player(String playerName) {
		this.name = playerName;

		dice = new Dice[5];
		for (int i = 0; i < 5; i++) {
			dice[i] = new Dice();
		}
	}

	public String getNameAndDeadJob() {
		return getDisplayName() + (!isAlive() ? (" (" + job.getJobName() + ")") : "");
	}

	public String getDisplayName() {
		return (job instanceof Sheriff ? "* " : "") + name;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String playerName) {
		this.name = playerName;
	}

	public int getArrowCount() {
		return arrowCount;
	}

	public void clearArrows() {
		arrowCount = 0;
	}

	public int getLivesLeft() {
		return livesLeft;
	}

	public boolean isAlive() {
		return livesLeft > 0;
	}

	public void incLife() {
		if (livesLeft < job.getMaxLives()) {
			livesLeft++;
		}
	}

	public void decLife(int lives) {
		livesLeft -= lives;
		if (livesLeft <= 0) {
			livesLeft = 0;
			Game.getInstance().returnArrows(this);
		}
	}

	public void decLife() {
		decLife(1);
	}

	public boolean areDiceAccepted() {
		return diceAccepted;
	}

	public int getThrowsLeft() {
		return throwsLeft;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
		clearArrows();
		livesLeft = job.getMaxLives();
		beginRound();
	}

	public int getDynamiteCount() {
		int counter = 0;
		for (Dice d : dice) {
			if (d.getValue() == Dice.Value.DYNAMITE) {
				counter++;
			}
		}
		return counter;
	}

	public boolean hasBeerForUse() {
		int counter = 0;
		for (Dice d : dice) {
			if (d.getValue() == Dice.Value.BEER) {
				counter++;
			}
		}
		return diceAccepted && counter > 0;
	}

	public void useUpBeer() {
		for (Dice d : dice) {
			if (d.getValue() == Dice.Value.BEER) {
				d.setNone();
				return;
			}
		}
	}

	public boolean canThrow() {
		return isAlive() && !diceAccepted && throwsLeft > 0 && getDynamiteCount() < 3;
	}

	private void applyDiceEffect() {
		for (Dice d : dice) {
			if (d.isReadyToThrow() && d.getValue() == Dice.Value.ARROW) {
				arrowCount++;
				Game.getInstance().decPileOfArrows();
				Game.getInstance().checkPileOfArrows();
			}
		}
		if (getDynamiteCount() >= 3) {
			decLife();
		}
	}

	public void throwDice() {
		if (throwsLeft > 0) {
			throwReadyDices();
			applyDiceEffect();
		}
	}

	private void throwReadyDices() {
		for (int i = 0; i < 5; i++) {
			if (dice[i].isReadyToThrow()) {
				dice[i].throwDice();
			}
		}
		throwsLeft--;
	}

	public void acceptDice() {
		diceAccepted = true;
	}

	public boolean checkIfWon() {
		for (Job target : job.getTargets()) {
			if (target.getPlayer().isAlive()) {
				return false;
			}
		}

		if (job instanceof Bandit) {
			boolean anyBanditOrViceAlive = false;
			for (Player p : Game.getInstance().players) {
				if ((p.getJob() instanceof Vice || p.getJob() instanceof Bandit) && p.isAlive()) {
					anyBanditOrViceAlive = true;
				}
			}
			return anyBanditOrViceAlive;
		}
		else {
			//TODO: Renegade a Vice a Šerif vyhráli, proč?
			return true;
		}
	}

	public void beginRound() {
		for (Dice d : dice) {
			d.reset();
		}
		throwsLeft = 3;
		diceAccepted = false;
	}

	public Dice getDice(int index) {
		return dice[index];
	}

	public boolean hasLongShotToUse() {
		// TODO stejny problem jako u hasBeerToUse, nejspis, e to copy/paste
		int counter = 0;
		for (Dice d : dice) {
			if (d.getValue() == Dice.Value.LONG_SHOT) {
				counter++;
			}
		}
		return diceAccepted && counter > 0;
	}

	public boolean hasShortShotToUse() {
		// TODO stejny problem jako u hasBeerToUse, nejspis, e to copy/paste
		int counter = 0;
		for (Dice d : dice) {
			if (d.getValue() == Dice.Value.SHORT_SHOT) {
				counter++;
			}
		}
		return diceAccepted && counter > 0;
	}

	public void useUpShortShot() {
		for (Dice d : dice) {
			if (d.getValue() == Dice.Value.SHORT_SHOT) {
				d.setNone();
				return;
			}
		}
	}

	public void useUpLongShot() {
		for (Dice d : dice) {
			if (d.getValue() == Dice.Value.LONG_SHOT) {
				d.setNone();
				return;
			}
		}
	}

	public boolean canGatling() {
		int counter = 0;
		for (Dice d : dice) {
			if (d.getValue() == Dice.Value.GATLING) {
				counter++;
			}
		}
		return diceAccepted && counter >= 3;
	}

	public void useUpGatling() {
		for (Dice d : dice) {
			if (d.getValue() == Dice.Value.GATLING) {
				d.setNone();
			}
		}
	}

	public boolean hasNoMoreToPlay() {
		boolean canPlay = canGatling();
		for (Dice d : dice) {
			switch (d.getValue()) {
				case BEER:
				case LONG_SHOT:
				case SHORT_SHOT:
					canPlay = true;
					break;
			}
		}
		return (!canPlay && diceAccepted) || !isAlive();
	}
}
