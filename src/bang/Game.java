package bang;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import jobses.Sheriff;
import jobses.Job;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import jobses.Bandit;
import jobses.Renegate;
import jobses.Vice;

public class Game {

	public static final int ARROW_MAX_COUNT = 9;
	
	public Player[] players;
	private int pileOfArrows = ARROW_MAX_COUNT;
	private static Game instance = null;

	public static Game getInstance() {
		if (instance == null) {
			instance = new Game();
		}
		return instance;
	}

	private Game() {
		initPlayers();
	}

	public final void initPlayers() {
		players = new Player[5];
		//nemenne pojmenovani hracu, jenom rozliseni cisly
		for (int i = 0; i < players.length; i++) {
			players[i] = new Player("Player " + (1 + i));
		}
	}

	public Player[] getPlayers() {
		return players;
	}

	public void setPlayers(Player[] players) {
		this.players = players;
	}

	public int getPileOfArrows() {
		return pileOfArrows;
	}

	public void resetPileOfArrows() {
		pileOfArrows = ARROW_MAX_COUNT;
	}

	/**
	 * Nastaví hráčům náhodná zaměstnání
	 *
	 * @return vrací index šerifa
	 */
	public int start() {
		int scheriffIndex = -1;

		Random r = new Random();
		List<Job> jobs = new LinkedList<>();

		Bandit bandit1 = new Bandit(null);
		Sheriff sheriff = new Sheriff(null);
		jobs.add(sheriff);
		jobs.add(bandit1);
		bandit1.addTarget(sheriff);
		sheriff.addTarget(bandit1);

		Renegate renegate = new Renegate(null);
		jobs.add(renegate);
		sheriff.addTarget(renegate);
		renegate.addTarget(sheriff);
		renegate.addTarget(bandit1);

		Bandit bandit2 = new Bandit(null);
		jobs.add(bandit2);
		bandit2.addTarget(sheriff);
		renegate.addTarget(bandit2);
		sheriff.addTarget(bandit2);

		Vice vice = new Vice(null);
		jobs.add(vice);
		vice.addTarget(bandit1);
		vice.addTarget(renegate);
		vice.addTarget(bandit2);
		//vice.getTargets().add(sheriff.getTargets());
		renegate.addTarget(vice);

		//prirazeni povolani
		for (int i = 0; i < players.length; i++) {
			Job job = jobs.get(r.nextInt(jobs.size()));
			players[i].setJob(job);
			job.setPlayer(players[i]);
			jobs.remove(job);

			if (players[i].getJob() instanceof Sheriff) {
				scheriffIndex = i;
			}
			//System.out.println("Zamestnani hrace " + players[i].getName() + " je " + players[i].getJob());
		}
		
		List<Integer> imgNums = new LinkedList<>();
		for (int i = 0; i < 10; i++) {
			imgNums.add(i);
		}
		for (Player p: players) {
			Integer num = imgNums.get(r.nextInt(imgNums.size()));
			imgNums.remove(num);
			try {
				p.setImage(new ImageIcon(ImageIO.read(new File("src/bangGUI/char0" + num + ".jpg"))));
			} catch (IOException ex) {
				System.err.println(ex.getMessage());
			}
		}
		
		
		return scheriffIndex;
	}

	public boolean isThereAWinner() {
		boolean someoneWon = false;
		for (int i = 0; i < players.length; i++) {
			Player player = players[i];
			if (player.checkIfWon()) {
				someoneWon = true;
				break;
			}
		}
		return someoneWon;
	}

	public void decPileOfArrows() {
		pileOfArrows--;
	}

	public void incPileOfArrows(int amount) {
		pileOfArrows += amount;
	}

	public String getWinnerList() {
		StringBuilder sb = new StringBuilder();
		for (Player p : players) {
			if (p.checkIfWon()) {
				sb.append("\n - ");
				sb.append(p.getDisplayName());
				sb.append(" (");
				sb.append(p.getJob().getJobName());
				sb.append(")");
			}
		}
		return sb.toString();
	}
	
	public String getWinnerJob() {
		for (Player p : players) {
			if (p.checkIfWon()) {
				return p.getJob().getJobName();
			}
		}
		return "None";
	}

	public void checkPileOfArrows() {
		if (getPileOfArrows() <= 0) {
			indiansAttack();
		}
	}

	public void indiansAttack() {
		for (Player p : players) {
			if (p.isAlive()) {
				p.decLife(p.getArrowCount());
				returnArrows(p);
			}
		}
	}
	
	public void gatlingAttack(int playerIndex) {
		players[playerIndex].useUpGatling();
		for (int i = 1; i < 5; i++) {
			Player p = players[(playerIndex + i) % 5];
			if (p.isAlive()) {
				p.decLife();
			}
		}
	}
	
	public void returnArrows(Player p) {
		int arrows = p.getArrowCount();
		p.clearArrows();
		incPileOfArrows(arrows);	
	}

}
