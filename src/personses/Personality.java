package personses;

abstract public class Personality {
    private String identification;
    private int numberOfLives;

    public Personality(String identification, int numberOfLives) {
        this.identification = identification;
        this.numberOfLives = numberOfLives;
    }
    
    public void lowerNumberOfLives() {
        numberOfLives--;        
    }

    public int getNumberOfLives() {
        return numberOfLives;
    }    
    
    abstract int increaseNumberOfLives(int numberOfLives);
}
