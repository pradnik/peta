package bangGUI;

import bang.Game;
import bang.Player;
import java.awt.Color;
import java.awt.event.ItemEvent;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.ImageIcon;

public class JFMain extends javax.swing.JFrame {

	public static final int PLAYER_COUNT = 5;
	private int currentPlayer = -1;
	private javax.swing.JCheckBox[] jCDice = null;

	private Player getCurrentPlayer() {
		return Game.getInstance().players[currentPlayer];
	}

	public JFMain() {
		initComponents();
		setLocationRelativeTo(null);

		jCDice = new JCheckBox[]{jCDice1, jCDice2, jCDice3, jCDice4, jCDice5};

		initNewGame();
	}

	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        jPPlayerInfo = new javax.swing.JPanel();
        jBRevealJob = new javax.swing.JButton();
        jLCurrentPlayerName = new javax.swing.JLabel();
        jLCurrentPlayerLivesLeft = new javax.swing.JLabel();
        jLCurrentPlayerArrowsCount = new javax.swing.JLabel();
        jCDice1 = new javax.swing.JCheckBox();
        jCDice5 = new javax.swing.JCheckBox();
        jCDice4 = new javax.swing.JCheckBox();
        jCDice3 = new javax.swing.JCheckBox();
        jCDice2 = new javax.swing.JCheckBox();
        jBThrow = new javax.swing.JButton();
        jBAcceptDice = new javax.swing.JButton();
        jLCurrentPlayerThrowsLeft = new javax.swing.JLabel();
        jBNextPlayer = new javax.swing.JButton();
        jBCurrentPlayerBeer = new javax.swing.JButton();
        imgCurrP = new javax.swing.JLabel();
        jPPlayer1 = new javax.swing.JPanel();
        jLPlayerName1 = new javax.swing.JLabel();
        jLLifesLeft1 = new javax.swing.JLabel();
        jLArrowCount1 = new javax.swing.JLabel();
        jBBeer1 = new javax.swing.JButton();
        jBShoot1 = new javax.swing.JButton();
        imgP1 = new javax.swing.JLabel();
        jPPlayer2 = new javax.swing.JPanel();
        jLPlayerName2 = new javax.swing.JLabel();
        jLLifesLeft2 = new javax.swing.JLabel();
        jLArrowCount2 = new javax.swing.JLabel();
        jBBeer2 = new javax.swing.JButton();
        jBShoot2 = new javax.swing.JButton();
        imgP2 = new javax.swing.JLabel();
        jPPlayer3 = new javax.swing.JPanel();
        jLPlayerName3 = new javax.swing.JLabel();
        jLLifesLeft3 = new javax.swing.JLabel();
        jLArrowCount3 = new javax.swing.JLabel();
        jBBeer3 = new javax.swing.JButton();
        jBShoot3 = new javax.swing.JButton();
        imgP3 = new javax.swing.JLabel();
        jPPlayer4 = new javax.swing.JPanel();
        jLPlayerName4 = new javax.swing.JLabel();
        jLLifesLeft4 = new javax.swing.JLabel();
        jLArrowCount4 = new javax.swing.JLabel();
        jBBeer4 = new javax.swing.JButton();
        jBShoot4 = new javax.swing.JButton();
        imgP4 = new javax.swing.JLabel();
        jLPileOfArrows = new javax.swing.JLabel();
        jBGatling = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMFile = new javax.swing.JMenu();
        jMenuNewGame = new javax.swing.JMenuItem();
        jMenuStats = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuExit = new javax.swing.JMenuItem();
        jMenuAbout = new javax.swing.JMenu();

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPPlayerInfo.setBackground(new java.awt.Color(248, 255, 181));
        jPPlayerInfo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        jBRevealJob.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jBRevealJob.setText("reveal job");
        jBRevealJob.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jBRevealJobMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jBRevealJobMouseReleased(evt);
            }
        });

        jLCurrentPlayerName.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLCurrentPlayerName.setText("jL");

        jLCurrentPlayerLivesLeft.setText("jLabel14");

        jLCurrentPlayerArrowsCount.setText("jLabel15");

        jCDice1.setSelected(true);
        jCDice1.setText("jCheckBox1");
        jCDice1.setEnabled(false);
        jCDice1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCDice1ItemStateChanged(evt);
            }
        });

        jCDice5.setSelected(true);
        jCDice5.setText("jCheckBox5");
        jCDice5.setEnabled(false);
        jCDice5.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCDice5ItemStateChanged(evt);
            }
        });

        jCDice4.setSelected(true);
        jCDice4.setText("jCheckBox4");
        jCDice4.setEnabled(false);
        jCDice4.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCDice4ItemStateChanged(evt);
            }
        });

        jCDice3.setSelected(true);
        jCDice3.setText("jCheckBox3");
        jCDice3.setEnabled(false);
        jCDice3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCDice3ItemStateChanged(evt);
            }
        });

        jCDice2.setSelected(true);
        jCDice2.setText("jCheckBox2");
        jCDice2.setEnabled(false);
        jCDice2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCDice2ItemStateChanged(evt);
            }
        });

        jBThrow.setText("Throw dice");
        jBThrow.setEnabled(false);
        jBThrow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBThrowActionPerformed(evt);
            }
        });

        jBAcceptDice.setText("Accept dice");
        jBAcceptDice.setEnabled(false);
        jBAcceptDice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAcceptDiceActionPerformed(evt);
            }
        });

        jLCurrentPlayerThrowsLeft.setText("jLabel16");

        jBNextPlayer.setText("Next player");
        jBNextPlayer.setEnabled(false);
        jBNextPlayer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBNextPlayerActionPerformed(evt);
            }
        });

        jBCurrentPlayerBeer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/Beer.png"))); // NOI18N
        jBCurrentPlayerBeer.setEnabled(false);
        jBCurrentPlayerBeer.setMaximumSize(null);
        jBCurrentPlayerBeer.setMinimumSize(null);
        jBCurrentPlayerBeer.setPreferredSize(new java.awt.Dimension(36, 36));
        jBCurrentPlayerBeer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCurrentPlayerBeerActionPerformed(evt);
            }
        });

        imgCurrP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/char04.jpg"))); // NOI18N

        javax.swing.GroupLayout jPPlayerInfoLayout = new javax.swing.GroupLayout(jPPlayerInfo);
        jPPlayerInfo.setLayout(jPPlayerInfoLayout);
        jPPlayerInfoLayout.setHorizontalGroup(
            jPPlayerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPPlayerInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPPlayerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCDice3)
                    .addComponent(jCDice4)
                    .addComponent(jCDice5)
                    .addComponent(jCDice1)
                    .addComponent(jCDice2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 230, Short.MAX_VALUE)
                .addGroup(jPPlayerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jBThrow, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLCurrentPlayerThrowsLeft, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPPlayerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jBNextPlayer, javax.swing.GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
                    .addComponent(jBAcceptDice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPPlayerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jBRevealJob, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPPlayerInfoLayout.createSequentialGroup()
                        .addGroup(jPPlayerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPPlayerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLCurrentPlayerName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLCurrentPlayerLivesLeft, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLCurrentPlayerArrowsCount, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jBCurrentPlayerBeer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(imgCurrP)))
                .addContainerGap())
        );
        jPPlayerInfoLayout.setVerticalGroup(
            jPPlayerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPPlayerInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPPlayerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPPlayerInfoLayout.createSequentialGroup()
                        .addComponent(jLCurrentPlayerThrowsLeft, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBThrow, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPPlayerInfoLayout.createSequentialGroup()
                        .addGroup(jPPlayerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPPlayerInfoLayout.createSequentialGroup()
                                .addComponent(jLCurrentPlayerName, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLCurrentPlayerLivesLeft, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLCurrentPlayerArrowsCount, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jBCurrentPlayerBeer, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPPlayerInfoLayout.createSequentialGroup()
                                .addGap(13, 13, 13)
                                .addComponent(jCDice1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCDice2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCDice3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCDice4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCDice5))
                            .addComponent(imgCurrP, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPPlayerInfoLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPPlayerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPPlayerInfoLayout.createSequentialGroup()
                                .addComponent(jBAcceptDice, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jBNextPlayer, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jBRevealJob, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jPPlayer1.setBackground(new java.awt.Color(255, 204, 153));
        jPPlayer1.setBorder(new javax.swing.border.MatteBorder(null));
        jPPlayer1.setPreferredSize(new java.awt.Dimension(134, 145));

        jLPlayerName1.setText("MNo");
        jLPlayerName1.setPreferredSize(new java.awt.Dimension(34, 14));

        jLLifesLeft1.setText("jLabel2");

        jLArrowCount1.setText("jLabel3");

        jBBeer1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/Beer.png"))); // NOI18N
        jBBeer1.setEnabled(false);
        jBBeer1.setMaximumSize(null);
        jBBeer1.setMinimumSize(null);
        jBBeer1.setPreferredSize(new java.awt.Dimension(36, 36));
        jBBeer1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBeer1ActionPerformed(evt);
            }
        });

        jBShoot1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/Short_shot.png"))); // NOI18N
        jBShoot1.setEnabled(false);
        jBShoot1.setMaximumSize(null);
        jBShoot1.setMinimumSize(null);
        jBShoot1.setPreferredSize(new java.awt.Dimension(36, 36));
        jBShoot1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBShoot1ActionPerformed(evt);
            }
        });

        imgP1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/char00.jpg"))); // NOI18N

        javax.swing.GroupLayout jPPlayer1Layout = new javax.swing.GroupLayout(jPPlayer1);
        jPPlayer1.setLayout(jPPlayer1Layout);
        jPPlayer1Layout.setHorizontalGroup(
            jPPlayer1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPPlayer1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPPlayer1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPPlayer1Layout.createSequentialGroup()
                        .addGroup(jPPlayer1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLPlayerName1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLLifesLeft1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLArrowCount1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                        .addComponent(imgP1))
                    .addGroup(jPPlayer1Layout.createSequentialGroup()
                        .addComponent(jBBeer1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBShoot1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPPlayer1Layout.setVerticalGroup(
            jPPlayer1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPPlayer1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPPlayer1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPPlayer1Layout.createSequentialGroup()
                        .addComponent(jLPlayerName1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLLifesLeft1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLArrowCount1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(imgP1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPPlayer1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBBeer1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBShoot1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPPlayer2.setBackground(new java.awt.Color(255, 204, 102));
        jPPlayer2.setBorder(new javax.swing.border.MatteBorder(null));
        jPPlayer2.setPreferredSize(new java.awt.Dimension(134, 145));

        jLPlayerName2.setText("jLabel4");

        jLLifesLeft2.setText("jLabel5");

        jLArrowCount2.setText("jLabel6");

        jBBeer2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/Beer.png"))); // NOI18N
        jBBeer2.setEnabled(false);
        jBBeer2.setMaximumSize(null);
        jBBeer2.setMinimumSize(null);
        jBBeer2.setPreferredSize(new java.awt.Dimension(36, 36));
        jBBeer2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBeer2ActionPerformed(evt);
            }
        });

        jBShoot2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/Long_shot.png"))); // NOI18N
        jBShoot2.setEnabled(false);
        jBShoot2.setMaximumSize(null);
        jBShoot2.setMinimumSize(null);
        jBShoot2.setPreferredSize(new java.awt.Dimension(36, 36));
        jBShoot2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBShoot2ActionPerformed(evt);
            }
        });

        imgP2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/char01.jpg"))); // NOI18N

        javax.swing.GroupLayout jPPlayer2Layout = new javax.swing.GroupLayout(jPPlayer2);
        jPPlayer2.setLayout(jPPlayer2Layout);
        jPPlayer2Layout.setHorizontalGroup(
            jPPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPPlayer2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPPlayer2Layout.createSequentialGroup()
                        .addGroup(jPPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLPlayerName2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLLifesLeft2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLArrowCount2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                        .addComponent(imgP2))
                    .addGroup(jPPlayer2Layout.createSequentialGroup()
                        .addComponent(jBBeer2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBShoot2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPPlayer2Layout.setVerticalGroup(
            jPPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPPlayer2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPPlayer2Layout.createSequentialGroup()
                        .addComponent(jLPlayerName2)
                        .addGap(18, 18, 18)
                        .addComponent(jLLifesLeft2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLArrowCount2))
                    .addComponent(imgP2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addGroup(jPPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBBeer2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBShoot2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPPlayer3.setBackground(new java.awt.Color(255, 204, 102));
        jPPlayer3.setBorder(new javax.swing.border.MatteBorder(null));
        jPPlayer3.setPreferredSize(new java.awt.Dimension(134, 145));

        jLPlayerName3.setText("jLabel7");

        jLLifesLeft3.setText("jLabel8");

        jLArrowCount3.setText("jLabel9");

        jBBeer3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/Beer.png"))); // NOI18N
        jBBeer3.setEnabled(false);
        jBBeer3.setMaximumSize(null);
        jBBeer3.setMinimumSize(null);
        jBBeer3.setPreferredSize(new java.awt.Dimension(36, 36));
        jBBeer3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBeer3ActionPerformed(evt);
            }
        });

        jBShoot3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/Long_shot.png"))); // NOI18N
        jBShoot3.setEnabled(false);
        jBShoot3.setMaximumSize(null);
        jBShoot3.setMinimumSize(null);
        jBShoot3.setPreferredSize(new java.awt.Dimension(36, 36));
        jBShoot3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBShoot3ActionPerformed(evt);
            }
        });

        imgP3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/char02.jpg"))); // NOI18N

        javax.swing.GroupLayout jPPlayer3Layout = new javax.swing.GroupLayout(jPPlayer3);
        jPPlayer3.setLayout(jPPlayer3Layout);
        jPPlayer3Layout.setHorizontalGroup(
            jPPlayer3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPPlayer3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPPlayer3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPPlayer3Layout.createSequentialGroup()
                        .addGroup(jPPlayer3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLPlayerName3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLLifesLeft3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLArrowCount3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                        .addComponent(imgP3))
                    .addGroup(jPPlayer3Layout.createSequentialGroup()
                        .addComponent(jBBeer3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBShoot3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPPlayer3Layout.setVerticalGroup(
            jPPlayer3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPPlayer3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPPlayer3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPPlayer3Layout.createSequentialGroup()
                        .addComponent(imgP3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                        .addComponent(jBShoot3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPPlayer3Layout.createSequentialGroup()
                        .addComponent(jLPlayerName3)
                        .addGap(18, 18, 18)
                        .addComponent(jLLifesLeft3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLArrowCount3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jBBeer3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPPlayer4.setBackground(new java.awt.Color(255, 204, 153));
        jPPlayer4.setBorder(new javax.swing.border.MatteBorder(null));
        jPPlayer4.setPreferredSize(new java.awt.Dimension(134, 145));

        jLPlayerName4.setText("jLabel10");

        jLLifesLeft4.setText("jLabel11");

        jLArrowCount4.setText("jLabel12");

        jBBeer4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/Beer.png"))); // NOI18N
        jBBeer4.setEnabled(false);
        jBBeer4.setMaximumSize(null);
        jBBeer4.setMinimumSize(null);
        jBBeer4.setPreferredSize(new java.awt.Dimension(36, 36));
        jBBeer4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBeer4ActionPerformed(evt);
            }
        });

        jBShoot4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/Short_shot.png"))); // NOI18N
        jBShoot4.setEnabled(false);
        jBShoot4.setMaximumSize(null);
        jBShoot4.setMinimumSize(null);
        jBShoot4.setPreferredSize(new java.awt.Dimension(36, 36));
        jBShoot4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBShoot4ActionPerformed(evt);
            }
        });

        imgP4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/char03.jpg"))); // NOI18N

        javax.swing.GroupLayout jPPlayer4Layout = new javax.swing.GroupLayout(jPPlayer4);
        jPPlayer4.setLayout(jPPlayer4Layout);
        jPPlayer4Layout.setHorizontalGroup(
            jPPlayer4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPPlayer4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPPlayer4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPPlayer4Layout.createSequentialGroup()
                        .addComponent(jBBeer4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBShoot4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPPlayer4Layout.createSequentialGroup()
                        .addGroup(jPPlayer4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLPlayerName4)
                            .addGroup(jPPlayer4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLArrowCount4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLLifesLeft4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                        .addComponent(imgP4)))
                .addContainerGap())
        );
        jPPlayer4Layout.setVerticalGroup(
            jPPlayer4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPPlayer4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPPlayer4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPPlayer4Layout.createSequentialGroup()
                        .addComponent(jLPlayerName4)
                        .addGap(18, 18, 18)
                        .addComponent(jLLifesLeft4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLArrowCount4))
                    .addComponent(imgP4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addGroup(jPPlayer4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBShoot4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBBeer4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLPileOfArrows.setText("jLabel16");
        jLPileOfArrows.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jBGatling.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bangGUI/Gatling.png"))); // NOI18N
        jBGatling.setEnabled(false);
        jBGatling.setPreferredSize(new java.awt.Dimension(36, 36));
        jBGatling.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGatlingActionPerformed(evt);
            }
        });

        jMFile.setText("File");

        jMenuNewGame.setText("NewGame");
        jMenuNewGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuNewGameActionPerformed(evt);
            }
        });
        jMFile.add(jMenuNewGame);

        jMenuStats.setText("Statistics");
        jMFile.add(jMenuStats);
        jMFile.add(jSeparator1);

        jMenuExit.setText("Exit");
        jMenuExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuExitActionPerformed(evt);
            }
        });
        jMFile.add(jMenuExit);

        jMenuBar1.add(jMFile);

        jMenuAbout.setText("About");
        jMenuAbout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuAboutMouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenuAbout);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPPlayerInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPPlayer1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jBGatling, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPPlayer2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 170, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPPlayer3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLPileOfArrows, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPPlayer4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPPlayer3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPPlayer2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(33, 33, 33)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLPileOfArrows)
                            .addComponent(jBGatling, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPPlayer4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPPlayer1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                .addComponent(jPPlayerInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuNewGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuNewGameActionPerformed
		initNewGame();
    }//GEN-LAST:event_jMenuNewGameActionPerformed

    private void jBBeer1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBeer1ActionPerformed
		getCurrentPlayer().useUpBeer();
		Game.getInstance().players[(currentPlayer + 1) % PLAYER_COUNT].incLife();
		updateControls();
    }//GEN-LAST:event_jBBeer1ActionPerformed

    private void jBBeer2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBeer2ActionPerformed
		getCurrentPlayer().useUpBeer();
		Game.getInstance().players[(currentPlayer + 2) % PLAYER_COUNT].incLife();
		updateControls();
    }//GEN-LAST:event_jBBeer2ActionPerformed

    private void jBBeer3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBeer3ActionPerformed
		getCurrentPlayer().useUpBeer();
		Game.getInstance().players[(currentPlayer + 3) % PLAYER_COUNT].incLife();
		updateControls();
    }//GEN-LAST:event_jBBeer3ActionPerformed

    private void jBBeer4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBeer4ActionPerformed
		getCurrentPlayer().useUpBeer();
		Game.getInstance().players[(currentPlayer + 4) % PLAYER_COUNT].incLife();
		updateControls();
    }//GEN-LAST:event_jBBeer4ActionPerformed

    private void jBShoot1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBShoot1ActionPerformed
		getCurrentPlayer().useUpShortShot();
		Game.getInstance().players[(currentPlayer + 1) % PLAYER_COUNT].decLife();
		updateControls();
		checkTheGameOver();
    }//GEN-LAST:event_jBShoot1ActionPerformed

    private void jBShoot2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBShoot2ActionPerformed
		getCurrentPlayer().useUpLongShot();
		Game.getInstance().players[(currentPlayer + 2) % PLAYER_COUNT].decLife();
		updateControls();
		checkTheGameOver();
    }//GEN-LAST:event_jBShoot2ActionPerformed

    private void jBShoot3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBShoot3ActionPerformed
		getCurrentPlayer().useUpLongShot();
		Game.getInstance().players[(currentPlayer + 3) % PLAYER_COUNT].decLife();
		updateControls();
		checkTheGameOver();
    }//GEN-LAST:event_jBShoot3ActionPerformed

    private void jBShoot4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBShoot4ActionPerformed
		getCurrentPlayer().useUpShortShot();
		Game.getInstance().players[(currentPlayer + 4) % PLAYER_COUNT].decLife();
		updateControls();
		checkTheGameOver();
    }//GEN-LAST:event_jBShoot4ActionPerformed

    private void jBGatlingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGatlingActionPerformed
		Game.getInstance().gatlingAttack(currentPlayer);
		updateControls();
		checkTheGameOver();
    }//GEN-LAST:event_jBGatlingActionPerformed

    private void jBCurrentPlayerBeerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCurrentPlayerBeerActionPerformed
		getCurrentPlayer().useUpBeer();
		Game.getInstance().players[currentPlayer].incLife();
		updateControls();
    }//GEN-LAST:event_jBCurrentPlayerBeerActionPerformed

    private void jBNextPlayerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBNextPlayerActionPerformed
		nextPlayer();
		updateControls();
    }//GEN-LAST:event_jBNextPlayerActionPerformed

    private void jBAcceptDiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAcceptDiceActionPerformed
		acceptDice();
		updateControls();
    }//GEN-LAST:event_jBAcceptDiceActionPerformed

    private void jBThrowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBThrowActionPerformed
		throwDice();
		checkTheGameOver();
		updateControls();
    }//GEN-LAST:event_jBThrowActionPerformed

    private void jCDice2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCDice2ItemStateChanged
		Game.getInstance().players[currentPlayer].getDice(1).setReadyToThrow(evt.getStateChange() == ItemEvent.SELECTED);
    }//GEN-LAST:event_jCDice2ItemStateChanged

    private void jCDice3ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCDice3ItemStateChanged
		Game.getInstance().players[currentPlayer].getDice(2).setReadyToThrow(evt.getStateChange() == ItemEvent.SELECTED);
    }//GEN-LAST:event_jCDice3ItemStateChanged

    private void jCDice4ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCDice4ItemStateChanged
		Game.getInstance().players[currentPlayer].getDice(3).setReadyToThrow(evt.getStateChange() == ItemEvent.SELECTED);
    }//GEN-LAST:event_jCDice4ItemStateChanged

    private void jCDice5ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCDice5ItemStateChanged
		Game.getInstance().players[currentPlayer].getDice(4).setReadyToThrow(evt.getStateChange() == ItemEvent.SELECTED);
    }//GEN-LAST:event_jCDice5ItemStateChanged

    private void jCDice1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCDice1ItemStateChanged
		Game.getInstance().players[currentPlayer].getDice(0).setReadyToThrow(evt.getStateChange() == ItemEvent.SELECTED);
    }//GEN-LAST:event_jCDice1ItemStateChanged

    private void jBRevealJobMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBRevealJobMouseReleased
		jBRevealJob.setText("reveal job");
    }//GEN-LAST:event_jBRevealJobMouseReleased

    private void jBRevealJobMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBRevealJobMousePressed
		jBRevealJob.setText(getCurrentPlayer().getJob().getJobNameAndMission());
    }//GEN-LAST:event_jBRevealJobMousePressed

    private void jMenuAboutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuAboutMouseClicked
		AboutDlg dlg = new AboutDlg(this, true);
		dlg.setLocationRelativeTo(this);
		dlg.setVisible(true);
    }//GEN-LAST:event_jMenuAboutMouseClicked

    private void jMenuExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuExitActionPerformed
		this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }//GEN-LAST:event_jMenuExitActionPerformed

	public static void main(String args[]) {
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
		 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(JFMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(JFMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(JFMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(JFMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		//</editor-fold>       
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new JFMain().setVisible(true);
			}
		});
	}


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel imgCurrP;
    private javax.swing.JLabel imgP1;
    private javax.swing.JLabel imgP2;
    private javax.swing.JLabel imgP3;
    private javax.swing.JLabel imgP4;
    private javax.swing.JButton jBAcceptDice;
    private javax.swing.JButton jBBeer1;
    private javax.swing.JButton jBBeer2;
    private javax.swing.JButton jBBeer3;
    private javax.swing.JButton jBBeer4;
    private javax.swing.JButton jBCurrentPlayerBeer;
    private javax.swing.JButton jBGatling;
    private javax.swing.JButton jBNextPlayer;
    private javax.swing.JButton jBRevealJob;
    private javax.swing.JButton jBShoot1;
    private javax.swing.JButton jBShoot2;
    private javax.swing.JButton jBShoot3;
    private javax.swing.JButton jBShoot4;
    private javax.swing.JButton jBThrow;
    private javax.swing.JCheckBox jCDice1;
    private javax.swing.JCheckBox jCDice2;
    private javax.swing.JCheckBox jCDice3;
    private javax.swing.JCheckBox jCDice4;
    private javax.swing.JCheckBox jCDice5;
    private javax.swing.JLabel jLArrowCount1;
    private javax.swing.JLabel jLArrowCount2;
    private javax.swing.JLabel jLArrowCount3;
    private javax.swing.JLabel jLArrowCount4;
    private javax.swing.JLabel jLCurrentPlayerArrowsCount;
    private javax.swing.JLabel jLCurrentPlayerLivesLeft;
    private javax.swing.JLabel jLCurrentPlayerName;
    private javax.swing.JLabel jLCurrentPlayerThrowsLeft;
    private javax.swing.JLabel jLLifesLeft1;
    private javax.swing.JLabel jLLifesLeft2;
    private javax.swing.JLabel jLLifesLeft3;
    private javax.swing.JLabel jLLifesLeft4;
    private javax.swing.JLabel jLPileOfArrows;
    private javax.swing.JLabel jLPlayerName1;
    private javax.swing.JLabel jLPlayerName2;
    private javax.swing.JLabel jLPlayerName3;
    private javax.swing.JLabel jLPlayerName4;
    private javax.swing.JMenu jMFile;
    private javax.swing.JMenu jMenuAbout;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuExit;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuNewGame;
    private javax.swing.JMenuItem jMenuStats;
    private javax.swing.JPanel jPPlayer1;
    private javax.swing.JPanel jPPlayer2;
    private javax.swing.JPanel jPPlayer3;
    private javax.swing.JPanel jPPlayer4;
    private javax.swing.JPanel jPPlayerInfo;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    // End of variables declaration//GEN-END:variables

	private void initNewGame() {
		PlayerNameDlg dlg = new PlayerNameDlg(this, true);
		dlg.setLocationRelativeTo(this);
		dlg.setVisible(true);
		
		currentPlayer = Game.getInstance().start();
		updateControls();
	}

	private void updateControls() {
		updatePlayerLabels();
		updateDiceCheckboxes();
		updateButtons();
		updateIcons();
	}

	private void updateIcons() {
		imgCurrP.setIcon(Game.getInstance().players[currentPlayer].getIcon());
		imgP1.setIcon(Game.getInstance().players[(currentPlayer + 1) % PLAYER_COUNT].getIcon());
		imgP2.setIcon(Game.getInstance().players[(currentPlayer + 2) % PLAYER_COUNT].getIcon());
		imgP3.setIcon(Game.getInstance().players[(currentPlayer + 3) % PLAYER_COUNT].getIcon());
		imgP4.setIcon(Game.getInstance().players[(currentPlayer + 4) % PLAYER_COUNT].getIcon());
	}

	private void updatePlayerLabels() {
		Player p = Game.getInstance().players[currentPlayer];
		jLCurrentPlayerName.setText(p.getNameAndDeadJob());
		jLCurrentPlayerLivesLeft.setText("Lives: " + p.getLivesLeft());
		jLCurrentPlayerArrowsCount.setText("Arrows: " + p.getArrowCount());
		jLCurrentPlayerThrowsLeft.setText("Throws left: " + p.getThrowsLeft());
		jPPlayerInfo.setBackground(p.isAlive() ? new Color(248, 255, 181) : Color.DARK_GRAY);

		p = Game.getInstance().players[(currentPlayer + 1) % PLAYER_COUNT];
		jLPlayerName1.setText(p.getNameAndDeadJob());
		jLLifesLeft1.setText("Lives: " + p.getLivesLeft());
		jLArrowCount1.setText("Arrows: " + p.getArrowCount());
		jPPlayer1.setBackground(p.isAlive() ? new Color(255, 204, 153) : Color.LIGHT_GRAY);

		p = Game.getInstance().players[(currentPlayer + 2) % PLAYER_COUNT];
		jLPlayerName2.setText(p.getNameAndDeadJob());
		jLLifesLeft2.setText("Lives: " + p.getLivesLeft());
		jLArrowCount2.setText("Arrows: " + p.getArrowCount());
		jPPlayer2.setBackground(p.isAlive() ? new Color(255, 204, 102) : Color.LIGHT_GRAY);

		p = Game.getInstance().players[(currentPlayer + 3) % PLAYER_COUNT];
		jLPlayerName3.setText(p.getNameAndDeadJob());
		jLLifesLeft3.setText("Lives: " + p.getLivesLeft());
		jLArrowCount3.setText("Arrows: " + p.getArrowCount());
		jPPlayer3.setBackground(p.isAlive() ? new Color(255, 204, 102) : Color.LIGHT_GRAY);

		p = Game.getInstance().players[(currentPlayer + 4) % PLAYER_COUNT];
		jLPlayerName4.setText(p.getNameAndDeadJob());
		jLLifesLeft4.setText("Lives: " + p.getLivesLeft());
		jLArrowCount4.setText("Arrows: " + p.getArrowCount());
		jPPlayer4.setBackground(p.isAlive() ? new Color(255, 204, 153) : Color.LIGHT_GRAY);

		jLPileOfArrows.setText("Arrows on pile: " + Game.getInstance().getPileOfArrows());

	}

	private void updateDiceCheckboxes() {
		for (int i = 0; i < 5; i++) {
			JCheckBox c = jCDice[i];
			c.setText(getCurrentPlayer().getDice(i).getValue().toString());
			boolean canBeRethrown = getCurrentPlayer().getDice(i).canBeRethrown();
			boolean readyToThrow = getCurrentPlayer().getDice(i).isReadyToThrow();
			boolean diceAccepted = getCurrentPlayer().areDiceAccepted();
			c.setSelected(canBeRethrown && readyToThrow);
			c.setEnabled(!diceAccepted && canBeRethrown && !getCurrentPlayer().getDice(i).isNone());
		}
	}

	private void updateButtons() {
		Player p = getCurrentPlayer();
		boolean p1Alive = Game.getInstance().players[(currentPlayer + 1) % PLAYER_COUNT].isAlive();
		boolean p2Alive = Game.getInstance().players[(currentPlayer + 2) % PLAYER_COUNT].isAlive();
		boolean p3Alive = Game.getInstance().players[(currentPlayer + 3) % PLAYER_COUNT].isAlive();
		boolean p4Alive = Game.getInstance().players[(currentPlayer + 4) % PLAYER_COUNT].isAlive();

		jBThrow.setEnabled(p.canThrow());
		jBAcceptDice.setEnabled(p.isAlive() && p.getThrowsLeft() < 3 && !p.areDiceAccepted());
		jBNextPlayer.setEnabled(p.hasNoMoreToPlay());

		boolean hasBeer = p.hasBeerForUse();
		jBCurrentPlayerBeer.setEnabled(hasBeer);
		jBBeer1.setEnabled(hasBeer && p1Alive);
		jBBeer2.setEnabled(hasBeer && p2Alive);
		jBBeer3.setEnabled(hasBeer && p3Alive);
		jBBeer4.setEnabled(hasBeer && p4Alive);

		boolean hasShort_Shot = p.hasShortShotToUse();
		jBShoot1.setEnabled(true/*hasShort_Shot && p1Alive*/);
		jBShoot4.setEnabled(true/*hasShort_Shot && p4Alive*/);

		boolean hasLong_Shot = p.hasLongShotToUse();
		jBShoot2.setEnabled(true/*hasLong_Shot && p2Alive*/);
		jBShoot3.setEnabled(true/*hasLong_Shot && p3Alive*/);

		boolean canGatling = p.canGatling();
		jBGatling.setEnabled(canGatling);

	}

	private void nextPlayer() {
		Player p;
		do {
			currentPlayer = (currentPlayer + 1) % PLAYER_COUNT;
			p = Game.getInstance().players[currentPlayer];
		} while (!p.isAlive());

		p.beginRound();
	}

	private void throwDice() {
		Player p = getCurrentPlayer();
		p.throwDice();
	}

	private void acceptDice() {
		getCurrentPlayer().acceptDice();
	}

	public void itemStateChanged(ItemEvent e) {
		Object source = e.getItemSelectable();
		int index = -1;

		if (source == jCDice1) {
			index = 0;
		} else if (source == jCDice2) {
			index = 1;
		} else if (source == jCDice3) {
			index = 2;
		} else if (source == jCDice4) {
			index = 3;
		} else if (source == jCDice5) {
			index = 4;
		}

		//Now that we know which button was pushed, find out
		//whether it was selected or deselected.
		if (e.getStateChange() == ItemEvent.DESELECTED) {
			Game.getInstance().players[currentPlayer].getDice(index).setReadyToThrow(false);
		}
	}

	private void checkTheGameOver() {
		if (Game.getInstance().isThereAWinner()) {
			showTheWinner();
			writeStats();
			initNewGame();
		}
	}

	private void showTheWinner() {
		JOptionPane.showMessageDialog(this, "Victory belongs to:\n" + Game.getInstance().getWinnerList());
	}

	private void writeStats() {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("stats.txt", true));
			bw.write(Game.getInstance().getWinnerJob() + "\n");
			bw.close();
		} catch (IOException ex) {
			System.err.println(ex.getMessage());
		}
	}
}
